# TIG Stack

Hello world example for TIG stack.

### Getting Started

```
docker-compose up -d
```

This will start the 3 containers for InfluxDB, Telegraf and Grafana. InfluxDB will create an initial database _telegraf_, an admin user and a telegraf user. Telegraf will push some standard metrics to InfluxDB. I added an input plugin to collect the CPU temperature. If the temperature is not correct, amend the following line in `telegraf.conf` (change the thermal zone):

```
commands = ["awk '{print $1/1000}' /sys/class/thermal/thermal_zone0/temp"]
```

I also added an input plugin to read from a csv file.

Grafana is running on port _3000_.
